#!/bin/bash
useradd --create-home ubuntu && \
mv /OpenSees /home/ubuntu/ && \
mkdir -p /home/ubuntu/lib && \
mkdir -p /home/ubuntu/bin && \
chown -R ubuntu:ubuntu /home/ubuntu/ && \
su - ubuntu && \
cd OpenSees/ && cp MAKES/Makefile.def.EC2-UBUNTU Makefile.def

